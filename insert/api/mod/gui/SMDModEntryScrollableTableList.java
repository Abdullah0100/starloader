package api.mod.gui;

/**
 * Created by Jake on 9/20/2020.
 * <insert description here>
 */

import api.mod.ModUpdater;
import api.mod.SMDModData;
import api.mod.SMDModInfo;
import api.utils.gui.SimplePopup;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.*;
import org.schema.schine.graphicsengine.forms.gui.newgui.*;
import org.schema.schine.input.InputState;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Set;

public class SMDModEntryScrollableTableList extends ScrollableTableList<SMDModInfo> implements GUIActiveInterface {

    private Collection<SMDModInfo> mods = new ArrayList<>();
    public boolean updated;

    public SMDModEntryScrollableTableList(InputState state, GUIElement element) {
        super(state, 750, 400, element);
        updateMods();
    }

    @Override
    public void initColumns() {
        new StringComparator();

        this.addColumn("Name", 4.0F, new Comparator<SMDModInfo>() {
            public int compare(SMDModInfo o1, SMDModInfo o2) {
                return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
            }
        });

        this.addColumn("Description", 8F, new Comparator<SMDModInfo>() {
            public int compare(SMDModInfo o1, SMDModInfo o2) {
                return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
            }
        });

        this.addColumn("Rating", 2F, new Comparator<SMDModInfo>() {
            public int compare(SMDModInfo o1, SMDModInfo o2) {
                return CompareTools.compare(o1.getRatingAverage(), o2.getRatingAverage());
            }
        });

        this.addColumn("Downloads", 2F, new Comparator<SMDModInfo>() {
            public int compare(SMDModInfo o1, SMDModInfo o2) {
                return CompareTools.compare(o1.getDownloadCount(), o2.getDownloadCount());
            }
        });

        this.addTextFilter(new GUIListFilterText<SMDModInfo>() {
            public boolean isOk(String s, SMDModInfo blueprint) {
                return blueprint.getName().toLowerCase().contains(s.toLowerCase());
            }
        }, ControllerElement.FilterRowStyle.FULL);

        this.activeSortColumnIndex = 0;
    }

    @Override
    protected Collection<SMDModInfo> getElementList() {
        if(!updated) updateMods();
        return mods;
    }

    public void updateMods() {
        mods.clear();
        for (SMDModInfo modDataValue : SMDModData.getInstance().getModDataValues()) {
            if(modDataValue.getTags().contains("starloader")) {
                mods.add(modDataValue);
            }
        }
        flagDirty();
        updated = true;
    }

    @Override
    public void updateListEntries(GUIElementList list, Set<SMDModInfo> set) {
        if(!updated) updateMods();
        for(final SMDModInfo mod : set) {
            GUITextOverlayTable nameTextElement = new GUITextOverlayTable(10, 10, this.getState());
            nameTextElement.setTextSimple(mod.getName());
            nameTextElement.setLimitTextDraw(30);
            GUIClippedRow nameRowElement;
            (nameRowElement = new GUIClippedRow(this.getState())).attach(nameTextElement);

            GUITextOverlayTable descriptionText = new GUITextOverlayTable(10, 10, this.getState());
            descriptionText.setTextSimple(mod.getTagLine());
            GUIClippedRow descriptionRowElement = new GUIClippedRow(this.getState());
            descriptionRowElement.attach(descriptionText);

            GUITextOverlayTable ratingText = new GUITextOverlayTable(10, 10, this.getState());
            ratingText.setTextSimple(mod.getRatingAverage() + " stars");
            GUIClippedRow ratingRowElement = new GUIClippedRow(this.getState());
            ratingRowElement.attach(ratingText);

            GUITextOverlayTable downloadsText = new GUITextOverlayTable(10, 10, this.getState());
            downloadsText.setTextSimple(mod.getDownloadCount() + " total downloads");
            GUIClippedRow downloadsRowElement = new GUIClippedRow(this.getState());
            downloadsRowElement.attach(downloadsText);

            final ModEntryListRow modListRow = new ModEntryListRow(this.getState(), mod, nameRowElement, descriptionRowElement, ratingText, downloadsText);
            modListRow.expanded = new GUIElementList(getState());
            GUIAncor buttonPane = new GUIAncor(getState(), 500, 28);

            GUITextButton downloadButton = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.OK, "DOWNLOAD", new GUICallback() {
                @Override
                public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                    if(mouseEvent.pressedLeftMouse()) {
                        if(modListRow.f != null) { //Probably not necessary to null check this but better safe than sorry...
                            getState().getController().queueUIAudio("0022_menu_ui - enter");
                            PlayerOkCancelInput confirmBox = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150, "Confirm Download", "Do you wish to download " + modListRow.f.getName() + "?") {
                                @Override
                                public void onDeactivate() {
                                }

                                @Override
                                public void pressedOK() {
                                    try {
                                        ModUpdater.downloadAndLoadMod(mod.getName());
                                        new SimplePopup(getState(), "Info", "Successfully downloaded mod: " + modListRow.f.getName());
                                    } catch (IOException | InstantiationException | InvocationTargetException | IllegalAccessException | ClassNotFoundException e) {
                                        e.printStackTrace();
                                        new SimplePopup(getState(), "Error", "Could not download " + modListRow.f.getName() + " due to an unexpected error.");
                                    }
                                }
                            };
                            confirmBox.getInputPanel().onInit();
                            confirmBox.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                            confirmBox.getInputPanel().background.setWidth((float) (GLFrame.getWidth() - 435));
                            confirmBox.getInputPanel().background.setHeight((float) (GLFrame.getHeight() - 70));
                            confirmBox.activate();
                        }
                    }
                }

                @Override
                public boolean isOccluded() {
                    return !isActive();
                }
            });

            buttonPane.attach(downloadButton);
            downloadButton.setPos(0, 0, 0);
            buttonPane.setPos(modListRow.expanded.getPos());
            modListRow.expanded.add(new GUIListElement(buttonPane, buttonPane, getState()));
            modListRow.expanded.attach(buttonPane);
            modListRow.onInit();

            list.addWithoutUpdate(modListRow);
        }

        list.updateDim();
    }

    public class ModEntryListRow extends ScrollableTableList<SMDModInfo>.Row {
        public ModEntryListRow(InputState inputState, SMDModInfo blueprint, GUIElement... guiElements) {
            super(inputState, blueprint, guiElements);
            this.highlightSelect = true;
            this.highlightSelectSimple = true;
            this.setAllwaysOneSelected(true);
        }
    }
}

