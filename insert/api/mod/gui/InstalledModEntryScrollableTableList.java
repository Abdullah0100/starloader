package api.mod.gui;

import api.mod.*;
import api.mod.StarMod;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.*;
import org.schema.schine.input.InputState;

import java.util.Collection;
import java.util.Comparator;
import java.util.Set;

public class InstalledModEntryScrollableTableList extends ScrollableTableList<StarMod> implements GUIActiveInterface {

    private Collection<StarMod> mods;
    public boolean updated;

    public InstalledModEntryScrollableTableList(InputState state, GUIElement element) {
        super(state, 750, 500, element);
        updateMods();
    }

    @Override
    public void initColumns() {
        new StringComparator();

        this.addColumn("Name", 4.0F, new Comparator<StarMod>() {
            public int compare(StarMod o1, StarMod o2) {
                return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
            }
        });

        this.addColumn("Description", 8F, new Comparator<StarMod>() {
            public int compare(StarMod o1, StarMod o2) {
                return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
            }
        });

        this.addColumn("Enabled", 2F, new Comparator<StarMod>() {
            public int compare(StarMod o1, StarMod o2) {
                ModDataFile.getInstance().isClientEnabled(o1.getName());
                return CompareTools.compare(ModDataFile.getInstance().isClientEnabled(o1.getName()), ModDataFile.getInstance().isClientEnabled(o2.getName()));
            }
        });

        this.addTextFilter(new GUIListFilterText<StarMod>() {
            public boolean isOk(String s, StarMod blueprint) {
                return blueprint.getName().toLowerCase().contains(s.toLowerCase());
            }
        }, ControllerElement.FilterRowStyle.FULL);

        this.activeSortColumnIndex = 0;
    }

    @Override
    protected Collection<StarMod> getElementList() {
        if(!updated) updateMods();
        return mods;
    }

    public void updateMods() {
        mods = StarLoader.starMods;
        flagDirty();
        updated = true;
    }

    @Override
    public void updateListEntries(GUIElementList list, Set<StarMod> set) {
        if(!updated) updateMods();
        for(final StarMod mod : set) {
            GUITextOverlayTable nameTextElement = new GUITextOverlayTable(10, 10, this.getState());
            nameTextElement.setTextSimple(mod.getName());
            nameTextElement.setLimitTextDraw(30);
            GUIClippedRow nameRowElement;
            (nameRowElement = new GUIClippedRow(this.getState())).attach(nameTextElement);

            GUITextOverlayTable descriptionText = new GUITextOverlayTable(10, 10, this.getState());
            descriptionText.setTextSimple(mod.getModDescription());
            GUIClippedRow descriptionRowElement = new GUIClippedRow(this.getState());
            descriptionRowElement.attach(descriptionText);

            GUITextOverlayTable enabledText = new GUITextOverlayTable(10, 10, this.getState());
            enabledText.setTextSimple(ModDataFile.getInstance().isClientEnabled(mod.getName()));
            GUIClippedRow ratingRowElement = new GUIClippedRow(this.getState());
            ratingRowElement.attach(enabledText);

            ModEntryListRow blueprintListRow = new ModEntryListRow(this.getState(), mod, nameRowElement, descriptionRowElement, enabledText);
            blueprintListRow.onInit();

            list.addWithoutUpdate(blueprintListRow);
        }

        list.updateDim();
    }

    public class ModEntryListRow extends ScrollableTableList<StarMod>.Row {
        public ModEntryListRow(InputState inputState, StarMod blueprint, GUIElement... guiElements) {
            super(inputState, blueprint, guiElements);
            this.highlightSelect = true;
            this.highlightSelectSimple = true;
            this.setAllwaysOneSelected(true);
        }
    }
}

