package api.mod.gui;

import api.DebugFile;
import api.mod.ModDataFile;
import api.mod.ModUpdater;
import api.mod.SMDModInfo;
import api.mod.StarMod;
import api.utils.gui.SimplePopup;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIAncor;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class ModBrowserPanel extends GUIElement implements GUIActiveInterface {

    private SMDModEntryScrollableTableList modList;
    private InstalledModEntryScrollableTableList installedModList;
    private boolean init;
    public GUIMainWindow mainPanel;
    private GUIContentPane modsTab;
    private GUIContentPane browseTab;
    private DialogInput dialogInput;
    private List<GUIElement> toCleanUp = new ObjectArrayList<>();

    public ModBrowserPanel(InputState state, DialogInput dialogInput) {
        super(state);
        this.dialogInput = dialogInput;
    }

    @Override
    public float getWidth() {
        return GLFrame.getWidth() - 470;
    }

    @Override
    public float getHeight() {
        return GLFrame.getHeight() - 70;
    }

    @Override
    public void cleanUp() {
        for(GUIElement e : toCleanUp) {
            e.cleanUp();
        }
        toCleanUp.clear();
    }

    @Override
    public void draw() {
        if(!init) {
            onInit();
        }
        GlUtil.glPushMatrix();
        transform();

        mainPanel.draw();

        GlUtil.glPopMatrix();
    }

    @Override
    public void onInit() {
        if(init) {
            return;
        }
        mainPanel = new GUIMainWindow(getState(), GLFrame.getWidth() - 410, GLFrame.getHeight() - 20, 400, 10, "Mod Manager");
        mainPanel.onInit();
        mainPanel.setPos(435, 35, 0);
        mainPanel.setWidth(GLFrame.getWidth() - 470);
        mainPanel.setHeight(GLFrame.getHeight() - 70);
        mainPanel.clearTabs();

        (modsTab = createInstalledTab()).onInit();
        (browseTab = createBrowseTab()).onInit();

        mainPanel.setSelectedTab(0);
        mainPanel.activeInterface = this;

        mainPanel.setCloseCallback(new GUICallback() {

            @Override
            public boolean isOccluded() {
                return !isActive();
            }

            @Override
            public void callback(GUIElement callingGuiElement, MouseEvent event) {
                if(event.pressedLeftMouse()){
                    dialogInput.deactivate();
                }
            }
        });

        toCleanUp.add(mainPanel);
        init = true;
    }

    @Override
    public boolean isInside() {
        return mainPanel.isInside();
    }

    private GUIContentPane createBrowseTab() {
        GUIContentPane contentPane = mainPanel.addTab("BROWSE");
        contentPane.setTextBoxHeightLast(880);
        modList = new SMDModEntryScrollableTableList(contentPane.getState(), contentPane.getContent(0));
        modList.onInit();
        contentPane.getContent(0).attach(modList);
        toCleanUp.add(modList);

        /* Moved Download Button to be part of expanded list
        contentPane.addNewTextBox(30);
        GUIAncor buttonPane = new GUIAncor(this.getState(), 500, 30);

        GUITextButton downloadButton = new GUITextButton(getState(), 250, 24, GUITextButton.ColorPalette.OK, "DOWNLOAD AND INSTALL", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    DebugFile.log("1");
                    if(modList.getSelectedRow() != null) {
                        DebugFile.log("2");
                        SMDModInfo f = modList.getSelectedRow().f;
                        DebugFile.log("3");
                        if (f != null) {
                            DebugFile.log("4");
                            getState().getController().queueUIAudio("0022_menu_ui - enter");
                            try {
                                ModUpdater.downloadAndLoadMod(f.getName());
                                new SimplePopup( getState(), "Successfully downloaded mod: " + f.getName());
                            } catch (IOException | InstantiationException | InvocationTargetException | IllegalAccessException | ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        });
        buttonPane.attach(downloadButton);
        downloadButton.setPos(0, 0, 0);

        contentPane.getContent(1).attach(buttonPane);
        toCleanUp.add(buttonPane);
         */
        toCleanUp.add(contentPane);
        return contentPane;
    }

    private GUIContentPane createInstalledTab() {
        GUIContentPane contentPane = mainPanel.addTab("INSTALLED");
        contentPane.setTextBoxHeightLast(850);
        installedModList = new InstalledModEntryScrollableTableList(contentPane.getState(), contentPane.getContent(0));
        installedModList.onInit();
        contentPane.getContent(0).attach(installedModList);
        toCleanUp.add(installedModList);

        contentPane.addNewTextBox(32);
        GUIAncor buttonPane = new GUIAncor(this.getState(), 500, 30);

        GUITextButton enable = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.OK, "ENABLE", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    if(installedModList.getSelectedRow() != null) {
                        StarMod f = installedModList.getSelectedRow().f;
                        if (f != null) {
                            getState().getController().queueUIAudio("0022_menu_ui - enter");
                            ModDataFile.getInstance().setClientEnabled(f.getName(), true);
                            installedModList.updateMods();
                            ModBrowserPanel.this.init = false;
                        }
                    }
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        });
        GUITextButton disable = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.CANCEL, "DISABLE", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    if(installedModList.getSelectedRow() != null) {
                        StarMod f = installedModList.getSelectedRow().f;
                        if (f != null) {
                            getState().getController().queueUIAudio("0022_menu_ui - enter");
                            ModDataFile.getInstance().setClientEnabled(f.getName(), false);
                            installedModList.updateMods();
                            ModBrowserPanel.this.init = false;
                        }
                    }
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        });
        buttonPane.attach(enable);
        enable.setPos(2, 2, 0);

        buttonPane.attach(disable);
        disable.setPos(2 + enable.getWidth() + 2, 2, 0);

        contentPane.getContent(1).attach(buttonPane);
        toCleanUp.add(buttonPane);
        toCleanUp.add(contentPane);
        return contentPane;
    }

    @Override
    public boolean isActive() {
        List<DialogInterface> playerInputs = getState().getController().getInputController().getPlayerInputs();
        return !MainMenuGUI.runningSwingDialog && (playerInputs.isEmpty() || playerInputs.get(playerInputs.size()-1).getInputPanel() == this);
    }
}
