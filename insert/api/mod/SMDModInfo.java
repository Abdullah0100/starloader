package api.mod;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

import java.util.ArrayList;

//Scraped info put into a class
public class SMDModInfo {
    private int resourceId;
    private int resourceDate;
    private String gameVersion;
    private int downloadCount;
    private float ratingAverage;
    private String tagLine;
    private ArrayList<String> tags;
    private String name;
    private String iconURL;

    public static SMDModInfo fromJson(JsonObject jsonModObject) {
        SMDModInfo inst = new SMDModInfo();
        inst.name = jsonModObject.get("title").getAsString();
        inst.resourceId = jsonModObject.get("resource_id").getAsInt();
        inst.resourceDate = jsonModObject.get("last_update").getAsInt();
        JsonElement element = jsonModObject.get("custom_fields").getAsJsonObject().get("Gameversion");
        if (element == null) {
            inst.gameVersion = "?";
        } else {
            inst.gameVersion = element.getAsString();
        }
        inst.downloadCount = jsonModObject.get("download_count").getAsInt();
        inst.ratingAverage = jsonModObject.get("rating_avg").getAsFloat();
        inst.tagLine = jsonModObject.get("tag_line").getAsString();
        JsonArray tags = jsonModObject.get("tags").getAsJsonArray();
        ArrayList<String> tagArray = new ArrayList<String>(tags.size());
        for (int i = 0; i < tags.size(); i++) {
            tagArray.add(tags.get(i).getAsString());
        }
        inst.tags = tagArray;
        JsonElement url = jsonModObject.get("icon_url");
        if (!(url instanceof JsonNull)) {
            inst.iconURL = url.getAsString();
        }

        return inst;
    }

    public int getResourceId() {
        return resourceId;
    }

    public int getResourceDate() {
        return resourceDate;
    }

    public String getGameVersion() {
        return gameVersion;
    }

    public int getDownloadCount() {
        return downloadCount;
    }

    public float getRatingAverage() {
        return ratingAverage;
    }

    public String getTagLine() {
        return tagLine;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public String getName() {
        return name;
    }

    public String getIconURL() {
        return iconURL;
    }

    @Override
    public String toString() {
        return "SMDModInfo{" +
                "resourceId=" + resourceId +
                ", resourceDate=" + resourceDate +
                ", gameVersion='" + gameVersion + '\'' +
                ", downloadCount=" + downloadCount +
                ", ratingAverage=" + ratingAverage +
                ", tagLine='" + tagLine + '\'' +
                ", tags=" + tags +
                ", name='" + name + '\'' +
                ", iconURL='" + iconURL + '\'' +
                '}';
    }
}
