package api.mod;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Collection;
import java.util.HashMap;

public class SMDModData {
    //Index all mods from SMD with Name -> [resource id, resource date]
    private static SMDModData instance;
    private final HashMap<String, SMDModInfo> allModData = new HashMap<String, SMDModInfo>();
    public static SMDModData getInstance() {
        if(instance == null){
            instance = new SMDModData();

        }
        return instance;
    }
    public SMDModInfo getModData(String name){
        return allModData.get(name);
    }

    private SMDModData(){
        JsonArray mods = SMDUtils.getSMDMods();
        if(mods == null) {
            return;
        }
        for (JsonElement jsonModElement : mods) {
            JsonObject jsonModObject = jsonModElement.getAsJsonObject();
            SMDModInfo info = SMDModInfo.fromJson(jsonModObject);
            allModData.put(info.getName(), info);
            System.out.println("[StarLoader SMDModData] Fetched mod from SMD: " + info);
        }
    }
    public int getLastUpdateDate(String modName){
        return allModData.get(modName).getResourceDate();
    }

    public static void main(String[] args) throws IOException {
        String hotkey = getInstance().getDownloadURL("Turret Hotkey");
        System.out.println(hotkey);
    }
    public String getDownloadURL(String modName) throws IOException {
        int resId = SMDModData.getInstance().getSMDId(modName);
        HttpURLConnection con = SMDUtils.GET("resources/" + resId);
        JsonParser parser = new JsonParser();
        String raw = IOUtils.toString(con.getInputStream(), "UTF-8");
        JsonObject parse = parser.parse(raw).getAsJsonObject();
        JsonObject resObject = parse.get("resource").getAsJsonObject();
        JsonArray currentFileArray = resObject.get("current_files").getAsJsonArray();
        JsonObject firstDownload = currentFileArray.get(0).getAsJsonObject();
        return firstDownload.get("download_url").getAsString();
    }

    public int getSMDId(String name) {
        return allModData.get(name).getResourceId();
    }

    public Collection<String> getModDataMap() {
        return allModData.keySet();
    }
    public Collection<SMDModInfo> getModDataValues(){
        return allModData.values();
    }
}
