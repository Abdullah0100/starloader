package api.tmp;

import api.mod.SMDUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class StarLoaderUpdater {
    //Project ID on GitLab
    public static final int STARLOADER_ID = 16765877;

    //Private key to a dummy account to work around a gitlab bug.
    //The private key can be made public and distributed with no consequences, as it has no relation or permissions to any project.
    public static final String PRIVATE_TOKEN = "gjdT7wVFvJ1ZaiJB1Xbb";

    public static HttpURLConnection GET(String request) throws IOException {
        URL url = new URL("https://gitlab.com/api/v4/projects/" + STARLOADER_ID + "/" + request);
        HttpURLConnection openConnection = (HttpURLConnection) url.openConnection();
        openConnection.setRequestMethod("GET");
        openConnection.setRequestProperty("PRIVATE-TOKEN", PRIVATE_TOKEN);

        System.out.println("RCode: " + openConnection.getResponseCode());
        System.out.println(openConnection.getResponseMessage());
        return openConnection;
//        System.out.println(text);
    }
    public static ArrayList<GitLabJob> jobs = new ArrayList<>();
    public static void getAllJobs() throws IOException {
        HttpURLConnection get = GET("jobs/");
        JsonArray resources = SMDUtils.getJsonArray(IOUtils.toString(get.getInputStream(), "UTF-8"));
        for (JsonElement resource : resources) {
            JsonObject obj = resource.getAsJsonObject();
            GitLabJob job = new GitLabJob(obj);
            jobs.add(job);
        }
    }
    public static GitLabJob getLatestJob(){
        assert !jobs.isEmpty();
        for (GitLabJob job : jobs) {
            if(job.status.equals("success")){
                return job;
            }
        }
        throw new RuntimeException("No gitlab job found!");
    }
    public static void downloadJob(GitLabJob job) throws IOException {
        int jId = job.id;
        //jobs/:job_id/artifacts/*artifact_path
        HttpURLConnection get = GET("jobs/" + jId + "/artifacts/build/libs/starloader.jar");
        FileUtils.copyInputStreamToFile(get.getInputStream(), new File("starloader.jar"));
        FileWriter writer = new FileWriter("starloader_job_info.txt");
        writer.write(jId + "\n");
        writer.write(job.toString());
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        getAllJobs();
        for (GitLabJob job : jobs) {
            System.out.println(job.toString());
        }
    }
    public static int getDownloadedJID() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File("starloader_job_info.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
        if(!scanner.hasNext()) return -1;
        String s = scanner.nextLine();
        try {
            return Integer.parseInt(s);
        }catch (NumberFormatException e){
            e.printStackTrace();
            return -1;
        }
    }
    public static List<String> getVMArguments(){
        return ManagementFactory.getRuntimeMXBean().getInputArguments();
    }
    public static void init(String[] args) throws IOException, InterruptedException {
        getAllJobs();
        GitLabJob latestJob = getLatestJob();
        System.err.println("Latest GitLab job: " + latestJob.toString());
        downloadJob(latestJob);

        FileUtils.copyFile(new File("StarMade.jar"), new File("patched_starmade.jar"));
        ArrayList<String> arguments = new ArrayList<>();
        arguments.add(getJavaInstallation());
        arguments.add("-jar");
        arguments.add("starloader.jar");
        arguments.add("-jar");
        arguments.add("patched_starmade.jar");
        ProcessBuilder pb = new ProcessBuilder(arguments);
        pb.directory(new File("."));
        Process p = pb.start();
        System.err.println(IOUtils.toString(p.getInputStream()));
        p.waitFor();
        System.err.println("OUTPUT CODE: " + p.exitValue());
        if(p.exitValue() != 0){
            System.err.println("!!! CRITICAL ERROR RUNNING INSTALLER, KILLING MAIN PROCESS !!!");
            System.exit(-1);
        }
        System.err.println("Done installing onto new jar");

        ArrayList<String> starArgs = new ArrayList<>();
        starArgs.add(getJavaInstallation());
        //Carry over VM arguments for memory and stuff
        starArgs.addAll(getVMArguments());
        starArgs.add("-jar");
        starArgs.add("patched_starmade.jar");
        //Do not run with patch
        starArgs.add("-nopatch");
        //Carry over program arguments
        starArgs.addAll(Arrays.asList(args));
        System.err.println("!! EXECUTING NEW JAR !!");
        ProcessBuilder startSM = new ProcessBuilder(starArgs);
        startSM.inheritIO();
        Process start = startSM.start();
        //Exit after done running starmade
        System.exit(0);

    }

    public static String getJavaInstallation() {
        String javaHome = System.getProperty("java.home");
        File f = new File(javaHome);
        f = new File(f, "bin");
        f = new File(f, "java.exe");
        System.err.println("Getting java path... " + f + "    exists: " + f.exists());
        return f.getPath();
    }
}
class GitLabJob {
    int id;
    String status;

    String shortId;
    String timeStamp;
    String author;
    String commitMessage;
    String web_url;

    public GitLabJob(JsonObject json) {
        this.id = json.get("id").getAsInt();
        this.status = json.get("status").getAsString();
        JsonObject cInfo = json.get("commit").getAsJsonObject();
        shortId = cInfo.get("short_id").getAsString();
        timeStamp = cInfo.get("created_at").getAsString();
        author = cInfo.get("author_name").getAsString();
        commitMessage = cInfo.get("title").getAsString();
        web_url = cInfo.get("web_url").getAsString();
    }

    @Override
    public String toString() {
        return "GitLabJob{" +
                "\nid=" + id +
                "\n, status='" + status + '\'' +
                "\n, shortId='" + shortId + '\'' +
                "\n, timeStamp='" + timeStamp + '\'' +
                "\n, author='" + author + '\'' +
                "\n, commitMessage='" + commitMessage + '\'' +
                "\n, web_url='" + web_url + '\'' +
                "\n}";
    }
}