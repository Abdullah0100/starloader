package api.utils.game;

import org.schema.game.common.controller.damage.projectile.ProjectileController;
import org.schema.game.common.controller.damage.projectile.ProjectileParticleContainer;
import org.schema.game.common.data.world.Sector;

import java.util.ArrayList;

public class SectorUtils {
    public static int[] getCannonProjectiles(Sector s){
        ProjectileController projectileController = s.getParticleController();
        int particleCount = projectileController.getParticleCount() + projectileController.getOffset();
        ProjectileParticleContainer container = s.getParticleController().getParticles();
        int[] r = new int[particleCount];
        for (int i = 0; i < particleCount; i++) {
            r[i] = i;
        }
        return r;
    }
}
