package api;

import api.common.GameClient;
import api.common.GameServer;
import api.config.BlockConfig;
import api.listener.Listener;
import api.listener.events.player.PlayerCommandEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.game.SectorUtils;
import api.utils.game.SegmentControllerUtils;
import api.utils.gui.SimpleGUIBuilder;
import com.bulletphysics.linearmath.Transform;
import it.unimi.dsi.fastutil.shorts.ShortOpenHashSet;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.projectile.ProjectileController;
import org.schema.game.common.controller.damage.projectile.ProjectileParticleContainer;
import org.schema.game.common.controller.elements.Pulse;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorElement;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.missile.Missile;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.Mesh;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.StateInterface;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class ModPlayground extends StarMod {
    public static void main(String[] args) {

    }

    @Override
    public void onGameStart() {
//        StarClassLoader loader = new StarClassLoader();
//        try {
//            loader.loadClass("org.schema.game.common.controller.elements.scanner.ScanAddOn");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
        setModName("DefaultMod").setModAuthor("Jake").setModDescription("test").setModVersion("1.0").setModSMVersion("0.202");
        setModDescription("Default mod that is always loaded");
        this.forceEnable = true;
    }

    public static short newCapId = 0;

    @Override
    public void onBlockConfigLoad(BlockConfig config) {
    }

    public static void initBlockData() {
        DebugFile.log("Initializing block data for enabled mods: ");
        final BlockConfig config = new BlockConfig();
        for (StarMod mod : StarLoader.starMods) {
            if (mod.isEnabled()) {
                DebugFile.log("Initializing block for mod: " + mod.getName());
                mod.onBlockConfigLoad(config);
            }
        }

        //Regenerate LOD shapes/Factory enhancers, rather than just obliterating the list in addElementToExisting
        for (Map.Entry<Short, ElementInformation> next : ElementKeyMap.informationKeyMap.entrySet()) {
            Short keyId = next.getKey();
            ElementKeyMap.lodShapeArray[keyId] = next.getValue().hasLod();
            ElementKeyMap.factoryInfoArray[keyId] = ElementKeyMap.getFactorykeyset().contains(keyId);
        }
        /*for (ElementInformation element : config.getElements()) {
            try {
                ElementKeyMap.addInformationToExisting(element);
            } catch (ParserConfigurationException e) {
                DebugFile.logError(e, null);
                e.printStackTrace();
            }
        }*/
    }

    public static void broadcastMessage(String message) {
        if (GameServerState.instance == null) {
            GameClient.sendMessage("[Client] " + message);
        } else {
            for (RegisteredClientOnServer client : GameServerState.instance.getClients().values()) {
                try {
                    client.serverMessage(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static SimpleGUIBuilder builder;

    @Override
    public void onEnable() {
        StarLoaderHooks.registerAllPackets();
        DebugFile.log("Loading default mod...");
//        StarLoader.registerListener(MainWindowTabAddEvent.class, new Listener<MainWindowTabAddEvent>() {
//            @Override
//            public void onEvent(MainWindowTabAddEvent event) {
//                if(event.getTitle().equals(Lng.ORG_SCHEMA_GAME_CLIENT_VIEW_GUI_FACTION_NEWFACTION_FACTIONPANELNEW_9)){
//                    GUIContentPane funny = event.createTab("this is a tab");
//                    GUITextOverlay t = new GUITextOverlay(100, 100, event.getPane().getState());
//                    t.setFont(FontLibrary.getBlenderProMedium20());
//                    t.setColor(Color.cyan);
//                    t.setTextSimple("jkdlsjfa sj kjdas skljklj dfasdfklj kl dfkljdfskljkljf kljdas klklj dfsklj dfskl ");
//                    funny.getContent(0).attach(t);
//                }
//            }
//        });

//        StarLoader.registerListener(PulseAddEvent.class, new Listener<PulseAddEvent>() {
//            @Override
//            public void onEvent(PulseAddEvent event) {
//                Pulse pulse = event.getPulse();
//                MyPulse p = new MyPulse(pulse.getState(),
//                        pulse.getWorldTransform(),
//                        ((SimpleTransformableSendableObject) pulse.getOwner()),
//                        10F,
//                        700,
//                        new Vector4f(0, 0.3F, 1, 0.6F));
//                event.setPulse(p);
//            }
//        });
//        new StarRunnable(){
//            @Override
//            public void run() {
//                if(GameClient.getClientState() == null) return;
//                PlayerState state = GameClient.getClientPlayerState();
//                if(state != null) {
//                    Transform t = new Transform();
//                    state.getWordTransform(t);
//                    state.sendClientMessage(t.origin.toString() + "", 0);
//                }
//            }
//        }.runTimer(1);
//        StarLoader.registerListener(BeamPostAddEvent.class, new Listener<BeamPostAddEvent>() {
//            @Override
//            public void onEvent(BeamPostAddEvent event) {
//                BeamState beam = event.getBeamState();
//                Sector hitSector = GameServer.getUniverse().getSector(beam.hitSectorId);
//                if(hitSector != null) {
//                    for (Pulse pulse : hitSector.getPulseController().getPulses()) {
//                        if (pulse instanceof MyPulse) {
//                            if (pulse.isActive()) {
//                                if (BeamPreAddEvent.beamIntersectsSphere(pulse.getWorldTransform().origin, pulse.currentRadius, beam.from, beam.to)) {
//                                    beam.setPower(beam.getPower() / 3F);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        });

        StarLoader.registerListener(PlayerCommandEvent.class, new Listener<PlayerCommandEvent>() {
            @Override
            public void onEvent(PlayerCommandEvent event) {
                PlayerControllable control = GameClient.getCurrentControl();
                if (control instanceof SegmentController) {
                    ManagedUsableSegmentController<?> seg = (ManagedUsableSegmentController<?>) control;
                    ReactorElement jumpChamber = SegmentControllerUtils.getChamberFromElement(seg, ElementKeyMap.getInfo(ElementKeyMap.REACTOR_CHAMBER_JUMP_DISTANCE_3));
                    if (jumpChamber != null && jumpChamber.isAllValid()) {
                        ModPlayground.broadcastMessage("yes!!!");
                    } else {
                        ModPlayground.broadcastMessage("no");
                    }
                }
            }
        });

//        StarLoader.registerListener(ControlManagerActivateEvent.class, new Listener<ControlManagerActivateEvent>() {
//            @Override
//            public void onEvent(ControlManagerActivateEvent event) {
//                if(!event.isActive()){
//                    builder.setVisible(false);
//                }
//            }
//        });
//
//        StarLoader.registerListener(KeyPressEvent.class, new Listener<KeyPressEvent>() {
//            @Override
//            public void onEvent(KeyPressEvent event) {
//                if(event.getChar() == 'b'){
//                    boolean visibility = !builder.isVisible();
//                    builder.setVisible(visibility);
//                    GameClient.getClientState().getGlobalGameControlManager().getIngameControlManager().getChatControlManager().setActive(visibility);
//                }
//            }
//        });
//
//        StarLoader.registerListener(PlayerGUICreateEvent.class, new Listener<PlayerGUICreateEvent>() {
//            @Override
//            public void onEvent(PlayerGUICreateEvent event) {
//
//                builder = SimpleGUIBuilder.newBuilder("Tab1")
//                        .fixedText("BR uh", Color.white, FontLibrary.getBlenderProMedium20())
//                        .newLine()
//                        .button("yo", new GUICallback() {
//                            @Override
//                            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
//
//                            }
//
//                            @Override
//                            public boolean isOccluded() {
//                                return false;
//                            }
//                        }).button("yo", new GUICallback() {
//                            @Override
//                            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
//
//                            }
//
//                            @Override
//                            public boolean isOccluded() {
//                                return false;
//                            }
//                        }).button("yo", new GUICallback() {
//                            @Override
//                            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
//
//                            }
//
//                            @Override
//                            public boolean isOccluded() {
//                                return false;
//                            }
//                        }).fixedText("yes", Color.blue, FontLibrary.getBoldArial24())
//                        .newLine().button("yo", new GUICallback() {
//                            @Override
//                            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
//
//                            }
//
//                            @Override
//                            public boolean isOccluded() {
//                                return false;
//                            }
//                        }).button("yo", new GUICallback() {
//                            @Override
//                            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
//
//                            }
//
//                            @Override
//                            public boolean isOccluded() {
//                                return false;
//                            }
//                        }).newLine().button("yo", new GUICallback() {
//                            @Override
//                            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
//
//                            }
//
//                            @Override
//                            public boolean isOccluded() {
//                                return false;
//                            }
//                        });
//                SimpleGUIList<Faction> guiBuilder = new SimpleGUIList<Faction>(GameClient.getClientState(), builder) {
//                    @Override
//                    public void initColumns() {
//                        addSimpleColumn("NAME", 8F, new RowStringCreator<Faction>() {
//                            @Override
//                            public String update(Faction entry) {
//                                return entry.getName();
//                            }
//                        });
//                        addSimpleColumn("yyyyy", 6F, new RowStringCreator<Faction>() {
//                            @Override
//                            public String update(Faction entry) {
//                                return String.valueOf(entry.getIdFaction());
//                            }
//                        });
//                        addSimpleColumn("online", 16F, new RowStringCreator<Faction>() {
//                            @Override
//                            public String update(Faction entry) {
//                                return String.valueOf(entry.getOnlinePlayers().size());
//                            }
//                        });
//                    }
//
//                    @Override
//                    protected Collection<Faction> getElementList() {
//                        return StarLoader.getGameState().getFactionManager().getFactionMap().values();
//                    }
//                };
//                guiBuilder.createEntryButton(GUITextButton.ColorPalette.NEUTRAL, "bruhhh", new EntryCallback<Faction>() {
//                    @Override
//                    public void on(GUIElement self, Faction entry, MouseEvent event) {
//
//                    }
//                });
//                guiBuilder.addDefaultFilter();
//
//                builder.addSimpleGUIList(guiBuilder);
//                builder.scaleCurrentLine();
//            }
//        });
//        StarLoader.registerListener(PlayerGUIDrawEvent.class, new Listener<PlayerGUIDrawEvent>() {
//            @Override
//            public void onEvent(PlayerGUIDrawEvent event) {
//                if(builder == null){
//                    DebugFile.warn("builder null oh boy");
//                    System.err.println("BUILDER NULL");
//                }else{
//                    builder.draw();
//                }
//            }
//        });

//        StarLoader.registerListener(MaxPowerCalculateEvent.class, new Listener() {
//            @Override
//            public void onEvent(Event event) {
//                MaxPowerCalculateEvent e = (MaxPowerCalculateEvent) event;
//                DebugFile.info("Original Power: " + e.getPower());
//                BatteryElementManager elementManager = e.getEntity().getElementManager(BatteryElementManager.class);
//                double extraPower = (double) elementManager.totalSize*100;
//                DebugFile.info("Extra Power: " + e.getPower());
//                e.setPower(e.getPower()+extraPower);
//            }
//        });
        getConfig("config").saveDefault("this is a: test");


//        StarLoader.registerListener(ControlManagerActivateEvent.class, new Listener<ControlManagerActivateEvent>() {
//            @Override
//            public void onEvent(ControlManagerActivateEvent event) {
//                broadcastMessage("Activated: " + event.getControlManager().getClass().getName() + " >>> " + event.isActive());
//                PlayerPanel playerPanel = GameClientState.instance.getWorldDrawer().getGuiDrawer().getPlayerPanel();
//                try {
//                    Field panel = PlayerPanel.class.getDeclaredField("factionPanelNew");
//                    panel.setAccessible(true);
//                    FactionPanelNew p = (FactionPanelNew) panel.get(playerPanel);
//                    if(!(p instanceof MyFactionPanelNew)) {
//                        GameClientState state = playerPanel.getState();
//                        panel.set(playerPanel, new MyFactionPanelNew(state));
//                    }
//                } catch (NoSuchFieldException ex) {
//                    ex.printStackTrace();
//                } catch (IllegalAccessException ex) {
//                    ex.printStackTrace();
//                }
//            }
//        }, null);


//        StarLoader.registerListener(PlayerCommandEvent.class, new Listener() {
//            @Override
//            public void onEvent(Event event) {
//                PlayerCommandEvent e = (PlayerCommandEvent) event;
//                Player p = e.player;
//                if(e.command.toLowerCase(Locale.ENGLISH).equals("test")){
//                    DebugFile.log("Test called", getMod());
//                    Entity currentEntity = p.getCurrentEntity();
//                    if(currentEntity == null){
//                        p.sendServerMessage("You are in: nothing, thanks for playing");
//                    }else{
//                        p.sendServerMessage("You are in: " + currentEntity.getUID());
//                    }
//                }else if(e.command.equals("thr")){
//                    Entity currentEntity = p.getCurrentEntity();
//                    if(currentEntity == null){
//                        p.sendServerMessage("no");
//                        return;
//                    }
//                    BatteryElementManager elementManager = currentEntity.getElementManager(BatteryElementManager.class);
//                    float actualThrust = elementManager.totalSize;
//                    Server.broadcastMessage("The total thrust of this object is: " + actualThrust);
//                }else if(e.command.equals("a")){
//                    p.sendPacket(new ServerToClientMessage(p.getFaction(), "hi"));
//                }
//            }
//        });
//
//
//        StarLoader.registerListener(HudCreateEvent.class, new Listener() {
//            @Override
//            public void onEvent(Event event) {
//                HudCreateEvent ev = (HudCreateEvent) event;
//                BasicInfoGroup bar = new BasicInfoGroup(ev);
//            }
//        });
//
//
////        final int[] t = {0};
////        new StarRunnable() {
////            @Override
////            public void run() {
////                t[0] += 4;
////            }
////        }.runTimer(1);
////        StarLoader.registerListener(CannonShootEvent.class, new Listener() {
////            @Override
////            public void onEvent(Event event) {
////                CannonShootEvent e = (CannonShootEvent) event;
////                Color hsb = Color.getHSBColor(((float) t[0] % 360) / 360F, 1F, 1F);
////                Vector4f tuple4f = new Vector4f(hsb.getRed() / 255F, hsb.getGreen() / 255F, hsb.getBlue() / 255F, 1F);
////                e.setColor(tuple4f);
////            }
////        });
//
    }
}

class MyPulse extends Pulse {
    //Transform location, Vector3f dir, SegmentController owner, float force, float radius, long weaponId, Vector4f pulseColor
    float r;
    int ran = 0;
    int originalSectorId;
    Transform originalLocation;

    public MyPulse(StateInterface anInterface, Transform transform, SimpleTransformableSendableObject object, float force, float rad, Vector4f color) {
        super(anInterface, (byte) 1, transform, object, new Vector3f(0, 5, 0), object, force, rad, object.getSectorId(), -1, color);
        r = rad;
        originalLocation = new Transform(transform);
        originalSectorId = object.getSectorId();
        collapseProgress = r - 100;
    }

    @Override
    public void draw(Mesh mesh) {
        super.draw(mesh);
    }

    float collapseProgress;

    @Override
    public void update(Timer timer) {
        if (this.isActive()) {
            super.update(timer);
            ran += 4;
            this.currentRadius = Math.min(ran, r - 200);
            this.getInitialTransform().set(originalLocation);
            final GameServerState serv = GameServer.getServerState();
            if (serv != null) {
                Sector sector = GameServer.getUniverse().getSector(originalSectorId);
                System.err.println("===== MISSILES ======");
                ArrayList<Missile> updatedMissiles = new ArrayList<>();
                ShortOpenHashSet missiles = sector.getMissiles();
                for (Short mId : missiles) {
                    Missile m = serv.getController().getMissileManager().getMissiles().get(mId);
                    if (m != null) {
                        Vector3f missileLoc = new Vector3f(m.getWorldTransform().origin);
                        missileLoc.sub(this.originalLocation.origin);
                        float distanceToCenterSquared = missileLoc.lengthSquared();
                        if (distanceToCenterSquared <= currentRadius * currentRadius) {
                            updatedMissiles.add(m);
                            System.err.println(m.getId());
                        }
                    }
                    //Speed, damage, distance
                }
                int SPECIAL_MISSILE_ID = -123;
                for (final Missile m : updatedMissiles) {
                    if (m.getWeaponId() != SPECIAL_MISSILE_ID) {
                        m.setDistance(0);
                        PlayerState p = null;
                        if(m.getShootingEntity() instanceof SegmentController){
                            ArrayList<PlayerState> players = SegmentControllerUtils.getAttachedPlayers((SegmentController) m.getShootingEntity());
                            if(!players.isEmpty()){
                                p = players.get(0);
                            }
                        }
                        if(p != null) {
                            Missile spawned = serv.getController().getMissileController().addDumbMissile(
                                    /*m.getOwner()*/ p,
                                    m.getWorldTransform(), m.getLinearVelocity(new Vector3f()), m.getSpeed(), m.getDamage()/2F, 3000, SPECIAL_MISSILE_ID, ((short) 6));
                        }
                    }
                }
                System.err.println("===== CANNON SHOTS =====");
                ProjectileController particleController = sector.getParticleController();
                ProjectileParticleContainer c = particleController.getParticles();
                int[] projectiles = SectorUtils.getCannonProjectiles(sector);
                for (int i = 0; i < particleController.getParticleCount(); i++) {
                    c.setDamage(i, c.getDamage(i)*0.9F);
                }
//                for (int projectile : projectiles) {
//                    particleController.deleteParticle(projectile);
//                }
                System.err.println("COUNT::: " + particleController.getParticleCount());
            }

            if (ran > 6000) {
                this.currentRadius = collapseProgress -= 10;
                if (collapseProgress <= 1) {
                    this.setActive(false);
                }
            }
        }
    }
}