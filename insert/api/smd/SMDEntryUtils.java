package api.smd;

import api.DebugFile;
import api.mod.SMDUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Jake on 9/19/2020.
 * <insert description here>
 */
public class SMDEntryUtils {
    private static boolean called = false;
    public static void fetchDataOnThread(){
        if(!called) {
            called = true;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        downloadBlueprintInfos();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //Set finished if it throws an exception
                    while (true) {
                        boolean b = finishedDownloading.compareAndSet(false, true);
                        if (b) break;
                    }
                }
            }).start();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    downloadTemplateInfos();
                }
            }).start();
        }
    }
    private final static AtomicBoolean finishedDownloading = new AtomicBoolean(false);

    public static boolean isFinishedDownloading() {
        return finishedDownloading.get();
    }

    private final static HashMap<String, SMDEntryData> nameData = new HashMap<>();
    private final static HashMap<String, SMDEntryData> templateData = new HashMap<>();

    private static JsonArray getCategory(int id){
        try {
//            get = SMDUtils.GET("");
            HttpURLConnection get = SMDUtils.GET("resource-categories/"+id+"/resources");//
            System.out.println("Response code from server: " + get.getResponseCode());
            return SMDUtils.getJsonArray(IOUtils.toString(get.getInputStream(), "UTF-8"), "resources");
        } catch (IOException e) {
            e.printStackTrace();
            DebugFile.log("Could not get resource-categories/2/resources from server");
        }
        return null;
    }
    public static final int[] BLUEPRINT_IDS = new int[]{2, 3, 7, 8}; // 2 = 11+12+15+more
    public static final int TEMPLATE_CATEGORY_ID = 9;
    public static void downloadBlueprintInfos() {
        for (int blueprintId : BLUEPRINT_IDS) {
            for (JsonElement resourceElement : getCategory(blueprintId)) {
                SMDEntryData data = new SMDEntryData(resourceElement.getAsJsonObject());
//                System.err.println("[StarLoader SMDBlueprintData] Fetched resource from SMD: " + data);
                nameData.put(data.title, data);
            }
        }
    }
    public static void downloadTemplateInfos() {
        for (JsonElement resourceElement : getCategory(TEMPLATE_CATEGORY_ID)) {
            SMDEntryData data = new SMDEntryData(resourceElement.getAsJsonObject());
//            System.err.println("[StarLoader SMDBlueprintData] Fetched resource from SMD: " + data);
            nameData.put(data.title, data);
        }
    }
    public static final HashMap<String, SMDEntryData> nullMap = new HashMap<>();
    static{
        SMDEntryData blankInstance = SMDEntryData.getBlankInstance();
        blankInstance.title = "Downloading Entries from StarmadeDock, please wait!";
        blankInstance.tag_line = "...";
        nullMap.put("Downloading Entries from StarmadeDock, please wait!", blankInstance);
    }
    public static HashMap<String, SMDEntryData> getAllBlueprints() {
        if(isFinishedDownloading()) {
            return nameData;
        }
        return nullMap;
    }

    public static HashMap<String, SMDEntryData> getAllTemplates() {
        if(isFinishedDownloading()) {
            return templateData;
        }
        return nullMap;
    }

    public static void main(String[] args) throws IOException {
        downloadBlueprintInfos();
    }
}
