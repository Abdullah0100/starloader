package api;

import org.schema.game.common.controller.elements.scanner.ScanAddOn;

import java.io.IOException;
import java.io.InputStream;

public class StarClassLoader extends ClassLoader {
    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        if (name.equals("org.schema.game.common.controller.elements.scanner.ScanAddOn")) {
            try {
                InputStream is = ScanAddOn.class.getClassLoader().getResourceAsStream("org/schema/game/common/controller/elements/scanner/ScanAddOn.class");
                byte[] buf = new byte[10000];
                int len = is.read(buf);
                return defineClass(name, buf, 0, len);
            } catch (IOException e) {
                throw new ClassNotFoundException("", e);
            }
        }
        return getParent().loadClass(name);
    }
}