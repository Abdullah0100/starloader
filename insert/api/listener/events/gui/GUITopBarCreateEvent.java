package api.listener.events.gui;

import api.listener.events.Event;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.game.client.view.gui.newgui.GUITopBar;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import java.util.ArrayList;

public class GUITopBarCreateEvent extends Event {

    private ArrayList<GUIHorizontalArea> buttons;
    private ArrayList<GUITopBar.ExpandedButton> dropdownButtons;
    private GUIHorizontalButtonTablePane taskPane;

    public GUITopBarCreateEvent(ArrayList<GUIHorizontalArea> buttons, ObjectArrayList<GUITopBar.ExpandedButton> dropdownButtons, GUIHorizontalButtonTablePane taskPane) {
        this.buttons = buttons;
        this.dropdownButtons = new ArrayList<>();
        this.dropdownButtons.addAll(dropdownButtons);
        this.taskPane = taskPane;
    }

    public GUIHorizontalButtonTablePane getTaskPane() {
        return taskPane;
    }

    public ArrayList<GUITopBar.ExpandedButton> getDropdownButtons() {
        return dropdownButtons;
    }

    public ArrayList<GUIHorizontalArea> getButtons() {
        return buttons;
    }
}
