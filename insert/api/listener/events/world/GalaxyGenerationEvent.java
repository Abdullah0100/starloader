package api.listener.events.world;

import api.listener.events.Event;

/**
 * STARMADE MOD
 * CREATOR: IR0NSIGHT
 * DATE: 25.08.2020
 * TIME: 15:48
 */
public class GalaxyGenerationEvent extends Event {
    /**
     * fired when a new universe is generated. holds all values needed to tweak the random generation
     * starCount, armCount, spread, range and rotation are vanilla fields
     * starChance is a modded field. 1/starChance stars will be created. starCount will not be met if starChance != 1
     * Current state: modifying values through event doesn't seem to work. don't know why.
     */
    private double starCount;
    private double armCount;
    private double spread;
    private double range;
    private double rotation;
    private float starRemoveRateOuter;
    private float starRemoveRateInner;
    private float starRemoveRateTransitionInnerBound; //The innermost radius to begin the transition
    private float starRemoveRateTransitionOuterBound; //The radius at which the transition stops; beyond this stars spawn 1/starRemoveRateOuter of the time.
    private boolean suppressCenterStarRemoval;

    public GalaxyGenerationEvent(double starCount, double armCount, double spread, double range, double rotation, float starRemoveRateInner, float starRemoveRateOuter, float starRemoveRateTransitionInnerBound, float starRemoveRateTransitionOuterBound, boolean suppressCenterStarRemoval) {
        this.starCount = starCount;
        this.armCount = armCount;
        this.spread = spread;
        this.range = range;
        this.rotation = rotation;
        this.starRemoveRateInner = starRemoveRateInner;
        this.starRemoveRateOuter = starRemoveRateOuter;
        this.starRemoveRateTransitionInnerBound = starRemoveRateTransitionInnerBound;
        this.starRemoveRateTransitionOuterBound = starRemoveRateTransitionOuterBound;
        this.suppressCenterStarRemoval = suppressCenterStarRemoval;
    }

    public void setStarCount(double c) {
        starCount = c;
    }

    public void  setArmCount(double c) {
        armCount = c;
    }

    public void setSpread (double s) {
        spread = s;
    }

    public void setRange (double r) {
        range = r;
    }

    public void setRotation(double r) {
        rotation = r;
    }

    public void setRemoveRateInner(float r) {
        starRemoveRateInner = r;
    }

    public void setRemoveRateOuter(float r) {
        starRemoveRateOuter = r;
    }

    public void setRemoveRateTransitionInnerBound(float s) {
        starRemoveRateTransitionInnerBound = s;
    }

    public void setRemoveRateTransitionOuterBound(float s) {
        starRemoveRateTransitionOuterBound = s;
    }

    public void setSuppressCenterStarRemoval(boolean s) { suppressCenterStarRemoval = s; }

    public double getArmCount() {
        return armCount;
    }

    public double getRange() {
        return range;
    }

    public double getRotation() {
        return rotation;
    }

    public double getSpread() {
        return spread;
    }

    public double getStarCount() {
        return starCount;
    }

    public float getRemoveRateInner() {
        return starRemoveRateInner;
    }

    public float getRemoveRateOuter() {
        return starRemoveRateOuter;
    }

    public float getRemoveRateTransitionInnerBound() {
        return starRemoveRateTransitionInnerBound;
    }

    public float getRemoveRateTransitionOuterBound() {
        return starRemoveRateTransitionOuterBound;
    }

    public boolean getSuppressCenterStarRemoval() {
        return suppressCenterStarRemoval;
    }
}
