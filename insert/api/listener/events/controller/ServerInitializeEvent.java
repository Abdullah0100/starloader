package api.listener.events.controller;

import api.listener.events.Event;
import org.schema.game.server.controller.GameServerController;
import org.schema.game.server.data.GameServerState;

/**
 * Created by Jake on 9/19/2020.
 * <insert description here>
 */
public class ServerInitializeEvent extends Event {
    private final GameServerController controller;
    private final GameServerState serverState;

    public ServerInitializeEvent(GameServerController controller, GameServerState serverState){

        this.controller = controller;
        this.serverState = serverState;
    }

    public GameServerController getController() {
        return controller;
    }

    public GameServerState getServerState() {
        return serverState;
    }
}
