package api;

import api.listener.events.systems.ReactorRecalibrateEvent;
import api.network.Packet;
import api.network.packets.PacketUtil;
import api.network.packets.ServerToClientMessage;
import api.utils.addon.PacketSCSyncSimpleAddOn;
import api.utils.addon.SimpleAddOn;
import api.utils.sound.PacketSCPlayAudio;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.PlayerUsableInterface;
import org.schema.game.common.controller.SegmentController;

/**
 * This class contains hooks for when the mod loader itself needs to use the event system
 * Its preferred to put event listeners here rather than when the event is fired or in ModPlayground
 * (For simplicity and to keep them organized)
 */
public class StarLoaderHooks {
    /**
     * StarLoader uses some packets to make modder's lives easier
     * They are registered in onEnable
     */
    public static void registerAllPackets(){
        PacketUtil.registerPacket(ServerToClientMessage.class);
        PacketUtil.registerPacket(PacketSCSyncSimpleAddOn.class);
        PacketUtil.registerPacket(PacketSCPlayAudio.class);
    }

    /**
     * SimpleAddOn's listen to reactor recalibrate very frequently, so its easier if every SAA is subscribed to it
     */
    public static void onReactorRecalibrateEvent(ReactorRecalibrateEvent event){
        SegmentController s = event.getImplementation().getSegmentController();
        if(s instanceof ManagedUsableSegmentController){
            ManagedUsableSegmentController<?> m = (ManagedUsableSegmentController<?>) s;
            for (PlayerUsableInterface addOn : m.getManagerContainer().getPlayerUsable()) {
                if(addOn instanceof SimpleAddOn){
                    ((SimpleAddOn) addOn).onReactorRecalibrate(event);
                }
            }
        }
    }
}
