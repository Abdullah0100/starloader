GUI
================
###Elements vs Containers
In StarMade's code, there are two distinct types of classes that make up it's GUIs. The first type are GUI Elements,
which will typically have the word GUI in their class name and extend the GUIElement class in one way or another. They
will contain functions like draw() and onInit(), which are called automatically by the second type - GUI Containers.
GUI Containers hold GUI Elements that have been attached to them and have methods like activate() and deactivate(). 
These methods control the creation, initialization, and drawing of attached GUI Elements.

#####Example of a GUIElement
    public class CatalogManagerPanel extends GUIElement implements GUIActiveInterface {
    
        private BlueprintEntryScrollableTableList blueprintList;
        private boolean init;
        public GUIMainWindow mainPanel;        
        ...
    
        public CatalogManagerPanel(InputState state, DialogInput dialogInput) {
            super(state);
            this.dialogInput = dialogInput;
        }
        
        ...
        
         @Override
         public void draw() {
             if(!init) {
                onInit();
             }
             GlUtil.glPushMatrix();
             transform();
        
             mainPanel.draw();
        
             GlUtil.glPopMatrix();
         }
        
         @Override
         public void onInit() {
             if(init) {
                return;
             }
             mainPanel = new GUIMainWindow(getState(), GLFrame.getWidth() - 410, GLFrame.getHeight() - 20, 400, 10, "Catalog Manager");
             mainPanel.onInit();
             ...
         }
    }

####Example of a GUIContainer
    public class CatalogManagerDialogMainMenu extends MainMenuInputDialog {
    
        private final CatalogManagerPanel managerPanel;
    
        public CatalogManagerDialogMainMenu(GameMainMenuController state) {
            super(state);
            ...
            managerPanel = new CatalogManagerPanel(state, this);
            managerPanel.onInit();
        }
    
        @Override
        public void handleMouseEvent(MouseEvent e) {
        }
    
        @Override
        public GUIElement getInputPanel() {
            return managerPanel;
        }
    
        @Override
        public void deactivate() {
            super.deactivate();
        }
    
        @Override
        public void onDeactivate() {
            managerPanel.cleanUp();
        }
    
        @Override
        public void update(Timer timer) {
            super.update(timer);
        }
        ...
    }
Note: Some parts of these classes have been omitted for the sake of this tutorial.